# Night Events

This webpage showcases various events hosted by Arizona's Disco. Crafted using HTML, CSS
and Javascript, the project prioritizes an enhanced user experience across diverse
devices, including laptops, smartphones, and tablets. Special attention has been
dedicated to optimizing web design for seamless navigation and engagement.

The project incorporates various enhancements including:

1) Employing media queries to ensure responsiveness across devices.
2) Leveraging Javascript for streamlined DOM manipulation.
3) Implementing HTML5 for a modern web layout.
4) Utilizing libraries such as Font Awesome and Bootstrap for enhanced functionality.
5) Introducing smooth transitions with the use of external libraries.

URL DEPLOY

https://nights-events.netlify.app/

